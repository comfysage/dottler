# About
Rebos is a tool that aims at mimicking what NixOS does (reproducibility), for any Linux distribution.

#### [Click to visit the wiki.](https://gitlab.com/Oglo12/rebos/-/wikis/home)
